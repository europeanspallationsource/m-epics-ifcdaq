# IFCDAQ

This is the NDS driver for the IOxOS IFC with a data aquisition FMC.

# Changelog

## 0.2.0

* Improve performance
* Implement trigger edge and level
* Implement gain

## 0.1.0

* Initial release
