/**
 * @file IFCDAQAIChannelGroup.h
 * @brief Header file defining the analog input channel group class.
 * @author nclaesson
 * @date 2016-03-29
 */

#ifndef IFCDAQAICHANNELGROUP_H
#define IFCDAQAICHANNELGROUP_H

#include <nds3/nds.h>

#include "IFCDAQAIChannel.h"

/**
 * @brief IFCDAQAIChannelGroup
 */
class IFCDAQAIChannelGroup {
public:
    IFCDAQAIChannelGroup(const std::string& name, nds::Node& parentNode, ifcdaqdrv_usr &deviceUser);

    nds::Port m_node;
    nds::StateMachine m_stateMachine;
    ifcdaqdrv_usr& m_deviceUser;
    uint32_t m_numChannels;
    std::vector<std::shared_ptr<IFCDAQAIChannel> > m_AIChannels;

    //void markAllParametersChanged();

    void getInfoMessage(timespec *timespec, std::string *value);
    void getTriggerButton(timespec *timespec, std::string *value);

    void setSamples(const timespec &timespec, const int32_t &value);
    void getSamples(timespec *timespec, int32_t *value);

    void setSampleRate(const timespec &timespec, const double &value);
    void getSampleRate(timespec *timespec, double *value);

    void getTriggerThreshold(timespec* timespec, double* value);
    void setTriggerThreshold(const timespec &timespec, const double &value);

    void getTriggerEdge(timespec* timespec, int32_t* value);
    void setTriggerEdge(const timespec &timespec, const int32_t &value);

    void getTriggerRepeat(timespec* timespec, int32_t* value);
    void setTriggerRepeat(const timespec &timespec, const int32_t &value);

    void getTriggerSource(timespec* timespec, int32_t* value);
    void setTriggerSource(const timespec &timespec, const int32_t &value);

    void getTriggerDelay(timespec* timespec, int32_t* value);
    void setTriggerDelay(const timespec &timespec, const int32_t &value);

    void getClockSource(timespec* timespec, int32_t* value);
    void setClockSource(const timespec &timespec, const int32_t &value);

    void getClockFrequency(timespec* timespec, double* value);
    void setClockFrequency(const timespec &timespec, const double &value);

    void getClockDivisor(timespec* timespec, int32_t* value);
    void setClockDivisor(const timespec &timespec, const int32_t &value);

    void getDecimation(timespec* timespec, int32_t* value);
    void setDecimation(const timespec &timespec, const int32_t &value);

    void getAveraging(timespec* timespec, int32_t* value);
    void setAveraging(const timespec &timespec, const int32_t &value);

    void onSwitchOn();
    void onSwitchOff();
    void onStart();
    void onStop();
    void recover();
    bool allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal);
    void commitParameters(bool calledFromAcquisitionThread = false);

private:
    int32_t *m_rawData;

    int32_t m_nSamples;
    double  m_sampleRate;

    double  m_triggerThreshold;
    int32_t m_triggerEdge;
    int32_t m_triggerRepeat;
    int32_t m_triggerSource;
    int32_t m_triggerDelay;

    int32_t m_clockSource;
    double  m_clockFrequency;
    int32_t m_clockDivisor;

    int32_t m_decimation;
    int32_t m_averaging;

    bool m_nSamplesChanged;
    bool m_sampleRateChanged;

    bool m_triggerThresholdChanged;
    bool m_triggerEdgeChanged;
    bool m_triggerRepeatChanged;
    bool m_triggerSourceChanged;
    bool m_triggerDelayChanged;

    bool m_clockSourceChanged;
    bool m_clockFrequencyChanged;
    bool m_clockDivisorChanged;

    bool m_decimationChanged;
    bool m_averagingChanged;

    nds::PVDelegateIn<std::string> m_infoPV;

    nds::PVDelegateIn<std::int32_t> m_nSamplesPV;
    nds::PVDelegateIn<double>      m_sampleRatePV;

    nds::PVDelegateIn<double>       m_triggerThresholdPV;
    nds::PVDelegateIn<std::int32_t> m_triggerEdgePV;
    nds::PVDelegateIn<std::int32_t> m_triggerRepeatPV;
    nds::PVDelegateIn<std::int32_t> m_triggerSourcePV;
    nds::PVDelegateIn<std::int32_t> m_triggerDelayPV;
    nds::PVVariableIn<std::int32_t> m_triggerCounterPV;

    nds::PVDelegateIn<std::int32_t> m_clockSourcePV;
    nds::PVDelegateIn<double> m_clockFrequencyPV;
    nds::PVDelegateIn<std::int32_t> m_clockDivisorPV;

    nds::PVDelegateIn<std::int32_t> m_decimationPV;
    nds::PVDelegateIn<std::int32_t> m_averagingPV;

    nds::Thread m_acquisitionThread;

    void acquisitionLoop(int32_t repeat);
    bool m_stop;
};
#if 0
class IFCDAQAIChannelGroup {

public:
    IFCDAQAIChannelGroup(const std::string& name);
    virtual ~IFCDAQAIChannelGroup();

    ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /* Parameters implemented from base class(es). */
    virtual ndsStatus setClockSource(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setClockFrequency(asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus setClockDivisor(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setSamplesCount(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTrigger(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTriggerCondition(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransferead);
    virtual ndsStatus onTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger);
    virtual ndsStatus setTriggerDelay(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setTriggerRepeat(asynUser* pasynUser, epicsInt32 value);

    virtual ndsStatus commitParameters();
    virtual ndsStatus markAllParametersChanged();
    virtual ndsStatus commitAllChannels();
    virtual ndsStatus resetAllAttenuators();

    ndsStatus setDeviceUser(sis8300drv_usr *newDeviceUser);
    ndsStatus checkSamplesConfig(epicsInt32 nsamples, epicsInt32 nchannels, epicsInt32 *nsamplesMax);
    int isDaqFinished();
    int getEnabledChannels();

protected:
    sis8300drv_usr *m_DeviceUser;                /**< User context for sis8300drv, set by NDS Device. */
    nds::Thread    *m_DaqTask;                   /**< NDS task/thread that waits for acquisition to finish. */
    int             m_DaqFinished;               /**< Flag signifying that the acquisition finished completely. >*/
    epicsInt32      m_TriggerRepeatRemaining;    /**< Remaining number of triggers to accept. */

    epicsFloat64        _PerfTiming;
    int                 _interruptIdPerfTiming;
    static std::string  PV_REASON_PERFTIMING;
    epicsFloat64        timemsec();

    std::string _TriggerConditionString;

    /* Flags to signify whether a parameter has been changed. */
    epicsInt32  _ClockSourceChanged;
    epicsInt32  _ClockFrequencyChanged;
    epicsInt32  _ClockDivisorChanged;
    epicsInt32  _SamplesCountChanged;
    epicsInt32  _TriggerConditionChanged;
    epicsInt32  _TriggerDelayChanged;
    epicsInt32  _TriggerRepeatChanged;

    virtual ndsStatus onSwitchProcessing(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);

    virtual ndsStatus handleStartMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStopMsg(asynUser *pasynUser, const nds::Message& value);

    ndsStatus daqTask(nds::TaskServiceBase &service);
};
#endif


#endif /* IFCDAQAICHANNELGROUP_H */
