
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "IFCDAQ.h"
#include "IFCDAQDevice.h"
#include "IFCDAQAIChannelGroup.h"

IFCDAQDevice::IFCDAQDevice(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters) :
        m_node(deviceName) {
    ifcdaqdrv_status status;

    m_deviceUser = {static_cast<uint32_t>(std::stoul(parameters.at("card"))),
                    static_cast<uint32_t>(std::stoul(parameters.at("fmc"))),
                    0};

    status = ifcdaqdrv_open_device(&m_deviceUser);
    if(status != status_success) {
      printf("Failed to open device %d\n", status);
        throw nds::NdsError("Failed to open device");
    }
    status = ifcdaqdrv_init_adc(&m_deviceUser);
    if(status != status_success) {
      printf("Failed to initialize ADC %d\n", status);
        throw nds::NdsError("Failed to initialize ADC");
    }

    std::shared_ptr<IFCDAQAIChannelGroup> aichgrp = std::make_shared<IFCDAQAIChannelGroup>("AI", m_node, m_deviceUser);
    m_AIChannelGroups.push_back(aichgrp);

    // Initialize device
    m_node.initialize(this, factory);
}

IFCDAQDevice::~IFCDAQDevice() {
    ifcdaqdrv_close_device(&m_deviceUser);
}

NDS_DEFINE_DRIVER(ifcdaq, IFCDAQDevice);
