
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "IFCDAQ.h"
#include "IFCDAQAIChannelGroup.h"
#include "IFCDAQAIChannel.h"

IFCDAQAIChannel::IFCDAQAIChannel(const std::string& name, nds::Node& parentNode, int32_t channelNum, ifcdaqdrv_usr & deviceUser, int32_t *rawData, double linConvFactor) :
    m_channelNum(channelNum),
    m_deviceUser(deviceUser),
    m_rawData(rawData),
    m_linConvFactor(0),
    m_linConvOffset(0),
    m_decimationFactor(1),
    m_decimationOffset(0),
    m_gain(1),
    m_pattern(0),
    m_linConvFactorNext(0),
    m_linConvOffsetNext(0),
    m_decimationFactorNext(1),
    m_decimationOffsetNext(0),
    m_gainNext(1),
    m_patternNext(0),
    m_linConvFactorChanged(true),
    m_linConvOffsetChanged(true),
    m_decimationFactorChanged(true),
    m_decimationOffsetChanged(true),
    m_gainChanged(true),
    m_patternChanged(true),
    m_dataPV(nds::PVDelegateIn<std::vector<double> >("Data",
                                                    std::bind(&IFCDAQAIChannel::getData,
                                                              this,
                                                              std::placeholders::_1,
                                                              std::placeholders::_2))),
    m_linConvFactorPV(nds::PVDelegateIn<double>("LinearConversionFactor-RB",
                                                    std::bind(&IFCDAQAIChannel::getLinearConversionFactor,
                                                              this,
                                                              std::placeholders::_1,
                                                              std::placeholders::_2))),
    m_linConvOffsetPV(nds::PVDelegateIn<double>("LinearConversionOffset-RB",
                                                    std::bind(&IFCDAQAIChannel::getLinearConversionOffset,
                                                              this,
                                                              std::placeholders::_1,
                                                              std::placeholders::_2))),
    m_decimationFactorPV(nds::PVDelegateIn<std::int32_t>("DecimationFactor-RB",
                                                    std::bind(&IFCDAQAIChannel::getDecimationFactor,
                                                              this,
                                                              std::placeholders::_1,
                                                              std::placeholders::_2))),
    m_decimationOffsetPV(nds::PVDelegateIn<std::int32_t>("DecimationOffset-RB",
                                                    std::bind(&IFCDAQAIChannel::getDecimationOffset,
                                                              this,
                                                              std::placeholders::_1,
                                                              std::placeholders::_2))),
    m_gainPV(nds::PVDelegateIn<double>("Gain-RB",
                                                    std::bind(&IFCDAQAIChannel::getGain,
                                                              this,
                                                              std::placeholders::_1,
                                                              std::placeholders::_2))),
    m_patternPV(nds::PVDelegateIn<std::int32_t>("Pattern-RB",
                                                    std::bind(&IFCDAQAIChannel::getPattern,
                                                              this,
                                                              std::placeholders::_1,
                                                              std::placeholders::_2)))
{
    //ifcdaqdrv_status status;
    m_node = parentNode.addChild(nds::Node(name));

    m_stateMachine = nds::StateMachine(true,
                                        std::bind(&IFCDAQAIChannel::switchOn, this),
                                        std::bind(&IFCDAQAIChannel::switchOff, this),
                                        std::bind(&IFCDAQAIChannel::start, this),
                                        std::bind(&IFCDAQAIChannel::stop, this),
                                        std::bind(&IFCDAQAIChannel::recover, this),
                                        std::bind(&IFCDAQAIChannel::allowChange, this,
                                                    std::placeholders::_1,
                                                    std::placeholders::_2,
                                                    std::placeholders::_3));

    m_node.addChild(m_stateMachine);

    m_dataPV.setMaxElements(CHANNEL_SAMPLES_MAX);
    m_dataPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_dataPV);

    /* Add Set-point PV for Linear Conversion Factor */
    m_node.addChild(nds::PVDelegateOut<double>("LinearConversionFactor",
                                                   std::bind(&IFCDAQAIChannel::setLinearConversionFactor,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannel::getLinearConversionFactor,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));
    /* Add Readback PV for Linear Conversion Factor */
    m_linConvFactorPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_linConvFactorPV);

    /* Add Set-point PV for Linear Conversion Offset */
    m_node.addChild(nds::PVDelegateOut<double>("LinearConversionOffset",
                                                   std::bind(&IFCDAQAIChannel::setLinearConversionOffset,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannel::getLinearConversionOffset,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));
    /* Add Readback PV for Linear Conversion Offset */
    m_linConvOffsetPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_linConvOffsetPV);

    /* Add Set-point PV for Decimation Factor */
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("DecimationFactor",
                                                   std::bind(&IFCDAQAIChannel::setDecimationFactor,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannel::getDecimationFactor,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));
    /* Add Readback PV for Decimation Factor */
    m_decimationFactorPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_decimationFactorPV);

    /* Add Set-point PV for Decimation Offset */
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("DecimationOffset",
                                                   std::bind(&IFCDAQAIChannel::setDecimationOffset,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannel::getDecimationOffset,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));
    /* Add Readback PV for Decimation Offset */
    m_decimationOffsetPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_decimationOffsetPV);

    /* Add Set-point PV for Gain */
    m_node.addChild(nds::PVDelegateOut<double>("Gain",
                                                   std::bind(&IFCDAQAIChannel::setGain,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannel::getGain,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));
    /* Add Readback PV for Gain */
    m_gainPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_gainPV);

    /* Add Set-point PV for Test Pattern */
    nds::enumerationStrings_t testPatternStrings;
    testPatternStrings.push_back("NORMAL");
    testPatternStrings.push_back("ZERO");
    testPatternStrings.push_back("ONE");
    testPatternStrings.push_back("TOGGLE");
    testPatternStrings.push_back("RAMP INC");
    testPatternStrings.push_back("RAMP DEC");
    testPatternStrings.push_back("8PSIN");
    nds::PVDelegateOut<std::int32_t> node(nds::PVDelegateOut<std::int32_t>("Pattern",
                                                   std::bind(&IFCDAQAIChannel::setPattern,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannel::getPattern,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));
    node.setEnumeration(testPatternStrings);
    m_node.addChild(node);

    /* Add Readback PV for Test Pattern */
    m_patternPV.setScanType(nds::scanType_t::interrupt);
    m_patternPV.setEnumeration(testPatternStrings);
    m_node.addChild(m_patternPV);

    /* Set some default values from hardware */
    uint32_t resolution;
    double vref_max;
    ifcdaqdrv_get_resolution(&m_deviceUser, &resolution);
    ifcdaqdrv_get_vref_max(&m_deviceUser, &vref_max);

    /* Calculate initial conversion factor to VREF MAX / INT_MAX or SHRT_MAX. */
    m_linConvFactor = m_linConvFactorNext = vref_max/((1 << (resolution - 1)) - 1);

    commitParameters();

}

void IFCDAQAIChannel::setLinearConversionFactor(const timespec &timespec, const double &value){
    m_linConvFactorNext = value;
    m_linConvFactorChanged = true;
    commitParameters();
}

void IFCDAQAIChannel::getLinearConversionFactor(timespec *timespec, double *value){
    *value = m_linConvFactor;
}


void IFCDAQAIChannel::setLinearConversionOffset(const timespec &timespec, const double &value){
    m_linConvOffsetNext = value;
    m_linConvOffsetChanged = true;
    commitParameters();
}

void IFCDAQAIChannel::getLinearConversionOffset(timespec *timespec, double *value){
    *value = m_linConvOffset;
}


void IFCDAQAIChannel::setDecimationFactor(const timespec &timespec, const int32_t &value){
    if(value < 1) {
        //IFCDAQNDS_MSGWRN("Decimation Factor cannot be less than 1.");
        return;
    }
    m_decimationFactorNext = value;
    m_decimationFactorChanged = true;
    commitParameters();
}

void IFCDAQAIChannel::getDecimationFactor(timespec *timespec, int32_t *value){
    *value = m_decimationFactor;
}


void IFCDAQAIChannel::setDecimationOffset(const timespec &timespec, const int32_t &value){
    if(value < 0) {
        //IFCDAQNDS_MSGWRN("Decimation Offset cannot be negative.");
        return;
    }
    m_decimationOffsetNext = value;
    m_decimationOffsetChanged = true;
    commitParameters();
}

void IFCDAQAIChannel::getDecimationOffset(timespec *timespec, int32_t *value){
    *value = m_decimationOffset;
}


void IFCDAQAIChannel::setGain(const timespec &timespec, const double &value){
    m_gainNext = value;
    m_gainChanged = true;
    commitParameters();
}

void IFCDAQAIChannel::getGain(timespec *timespec, double *value){
    *value = m_gain;
}


void IFCDAQAIChannel::setPattern(const timespec &timespec, const int32_t &value){
    m_patternNext = value;
    m_patternChanged = true;
    commitParameters();
}

void IFCDAQAIChannel::getPattern(timespec *timespec, int32_t *value){
    *value = m_pattern;
}

void IFCDAQAIChannel::commitParameters(bool calledFromAcquisitionThread){
    struct timespec now = {0, 0};
    ifcdaqdrv_status status;

    clock_gettime(CLOCK_REALTIME, &now);

    /* If the call comes from the acquisition thread it is certain that the device
     * is not armed. Otherwise, only allow changes if device is in on or going to on. */

    if(!calledFromAcquisitionThread && (
            m_stateMachine.getLocalState() != nds::state_t::on &&
            m_stateMachine.getLocalState() != nds::state_t::stopping  &&
            m_stateMachine.getLocalState() != nds::state_t::initializing )) {
        return;
    }

    if(m_linConvFactorChanged) {
        m_linConvFactorChanged = false;
        m_linConvFactor = m_linConvFactorNext;
        double tmp;
        m_linConvFactorPV.read(&now, &tmp);
        m_linConvFactorPV.push(now, tmp);
    }

    if(m_linConvOffsetChanged) {
        m_linConvOffsetChanged = false;
        m_linConvOffset = m_linConvOffsetNext;
        double tmp;
        m_linConvOffsetPV.read(&now, &tmp);
        m_linConvOffsetPV.push(now, tmp);
    }

    if(m_decimationFactorChanged) {
        m_decimationFactorChanged = false;
        m_decimationFactor = m_decimationFactorNext;
        int32_t tmp;
        m_decimationFactorPV.read(&now, &tmp);
        m_decimationFactorPV.push(now, tmp);
    }

    if(m_decimationOffsetChanged) {
        m_decimationOffsetChanged = false;
        m_decimationOffset = m_decimationOffsetNext;
        int32_t tmp;
        m_decimationOffsetPV.read(&now, &tmp);
        m_decimationOffsetPV.push(now, tmp);
    }

    if(m_gainChanged) {
        m_gainChanged = false;
        status = ifcdaqdrv_set_gain(&m_deviceUser, m_channelNum, m_gainNext);
        if(!status) {
            double change = m_gain;
            ifcdaqdrv_get_gain(&m_deviceUser, m_channelNum, &m_gain);
            change = change / m_gain;
            m_linConvFactor = m_linConvFactorNext = m_linConvFactorNext * change;

            m_gainPV.push(now, m_gain);
            m_linConvFactorPV.push(now, m_linConvFactor);
        }
    }

    if(m_patternChanged) {
        m_patternChanged = false;
        status = ifcdaqdrv_set_pattern(&m_deviceUser, m_channelNum, (ifcdaqdrv_pattern)m_patternNext);
        if(!status) {
            m_pattern = m_patternNext;
            int32_t tmp;
            m_patternPV.read(&now, &tmp);
            m_patternPV.push(now, tmp);
        }
    }

}

void IFCDAQAIChannel::switchOn()
{
    m_decimationOffsetNext = 0;
    m_decimationOffsetChanged = true;
    m_decimationFactorNext = 1;
    m_decimationFactorChanged = true;
    commitParameters();
}
void IFCDAQAIChannel::switchOff()
{
}
void IFCDAQAIChannel::start()
{
    m_firstReadout = true;
}
void IFCDAQAIChannel::stop()
{
    commitParameters();
}

/** @brief Don't support recover */
void IFCDAQAIChannel::recover()
{
    throw nds::StateMachineRollBack("Cannot recover");
}

/** @brief Allow all transitions */
bool IFCDAQAIChannel::allowChange(const nds::state_t, const nds::state_t, const nds::state_t)
{
    return true;
}


void IFCDAQAIChannel::read(int32_t *data, int32_t nSamples, uint32_t nSamplesHw, int32_t nPretrig, uint32_t nPretrigHw)
{
    if(m_stateMachine.getLocalState() != nds::state_t::running)
    {
        /* Print the warning message the first time this function is inappropriately called */
        if(m_firstReadout) {
            m_firstReadout = false;
            ndsDebugStream(m_node) << "Channel " << m_channelNum << " not Running" << std::endl;
        }
        return;
    }

    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);

    m_data.clear();
    m_data.reserve(nSamples);

    int32_t *itr;
    int32_t *origin;

    std::vector<double>::iterator target = m_data.begin();

    origin = data + (nPretrigHw - nPretrig);

    if(m_decimationFactor != 1) {
        m_data.resize(nSamples/m_decimationFactor);
        for(itr = origin + m_decimationOffset; itr < origin + nSamples; itr += m_decimationFactor, ++target){
            *target = (double)*itr * m_linConvFactor + m_linConvOffset;
        }
    } else {
        m_data.resize(nSamples);
        for(itr = origin + m_decimationOffset; itr < origin + nSamples; ++itr, ++target){
            *target = (double)*itr * m_linConvFactor + m_linConvOffset;
        }
    }

    m_dataPV.push(now, m_data);

    commitParameters(true);
}

void IFCDAQAIChannel::getData(timespec *timespec, std::vector<double> *value)
{
    /*
    clock_gettime(CLOCK_REALTIME, timespec);

    for(size_t i = 0; i< 100; i++) {
        value->push_back((double) 10 * (m_rawData[i]/SHRT_MAX) );
    }
    */
}

void IFCDAQAIChannel::setState(nds::state_t newState)
{
    m_stateMachine.setState(newState);
}
