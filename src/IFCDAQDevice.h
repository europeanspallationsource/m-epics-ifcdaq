#ifndef IFCDAQDEVICE_H
#define IFCDAQDEVICE_H

#include <nds3/nds.h>

#include <ifcdaqdrv.h>

#include "IFCDAQAIChannelGroup.h"
#include "IFCDAQAIChannel.h"

class IFCDAQDevice
{
  public:
    IFCDAQDevice(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters);
    ~IFCDAQDevice();

  private:
    std::vector<std::shared_ptr<IFCDAQAIChannelGroup> > m_AIChannelGroups;
    struct ifcdaqdrv_usr m_deviceUser;
    nds::Node m_node;
};

#endif /* IFCDAQDEVICE_H */
