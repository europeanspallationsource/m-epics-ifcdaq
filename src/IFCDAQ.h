#ifndef IFCDAQ_H
#define IFCDAQ_H


#include <iostream>
#include <sstream>

/*
 * Helper macro to push text to user through the INFO pv
 *
 * @param text Has to be something that can be passed to std::string()
 */

#define IFCDAQNDS_MSGWRN(text)                         \
    do {                                               \
        struct timespec now;                           \
        clock_gettime(CLOCK_REALTIME, &now);           \
        m_infoPV.push(now, std::string(text));                    \
        ndsWarningStream(m_node) << std::string(text) << "\n";    \
    } while(0)

/** Status check helper that prints error message from userspace library,
 * sends a message to the MSGR record and returns ndsError. */
#define IFCDAQNDS_STATUS_MSGERR(func, status)                      \
    do {                                                           \
        std::ostringstream tmp;                                    \
        tmp << (func) << " " <<                                    \
                    ifcdaqdrv_strerror(status) << (status);        \
        IFCDAQNDS_MSGWRN(tmp.str());                               \
    } while(0)

#define IFCDAQNDS_STATUS_CHECK(func, status)                                \
    do {                                                                    \
        if ((status) != status_success) {                                   \
            IFCDAQNDS_STATUS_MSGERR(func, status);                          \
            /*throw nds::NdsError("ifcdaqdrv returned error");*/            \
        }                                                                   \
    } while (0)



#define CHANNEL_NUMBER_MAX 8
// TODO: reallocate with every commitParameters instead. Issue: channels pointers has to be updated with every realloc.
#define CHANNEL_SAMPLES_MAX (4*1024*1024)

#endif /* IFCDAQ_H */
