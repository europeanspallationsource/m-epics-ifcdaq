#ifndef IFCDAQAICHANNEL_H
#define IFCDAQAICHANNEL_H

#include <nds3/nds.h>

class IFCDAQAIChannel
{
public:
    IFCDAQAIChannel(const std::string& name, nds::Node& parentNode, int32_t channelNum, struct ifcdaqdrv_usr &deviceUser, int32_t *rawData, double linConvFactor);

    std::int32_t m_channelNum;

    void read(int32_t *data, int32_t nSamples, uint32_t nSamplesHw, int32_t nPretrig, uint32_t nPretrigHw);
    void getData(timespec *timespec, std::vector<double> *value);

    void setState(nds::state_t newState);

    void setLinearConversionFactor(const timespec &timespec, const double &value);
    void getLinearConversionFactor(timespec *timespec, double *value);

    void setLinearConversionOffset(const timespec &timespec, const double &value);
    void getLinearConversionOffset(timespec *timespec, double *value);

    void setDecimationFactor(const timespec &timespec, const int32_t &value);
    void getDecimationFactor(timespec *timespec, int32_t *value);

    void setDecimationOffset(const timespec &timespec, const int32_t &value);
    void getDecimationOffset(timespec *timespec, int32_t *value);

    void setGain(const timespec &timespec, const double &value);
    void getGain(timespec *timespec, double *value);

    void setPattern(const timespec &timespec, const int32_t &value);
    void getPattern(timespec *timespec, int32_t *value);

    void commitParameters(bool calledFromAcquisitionThread = false);

private:
    struct ifcdaqdrv_usr &m_deviceUser;
    int32_t *m_rawData;
    std::vector<double> m_data;
    nds::Node m_node;
    nds::StateMachine m_stateMachine;

    bool m_firstReadout; /* Flag to indicate that this is the first time data is read out. */

    void switchOn();
    void switchOff();
    void start();
    void stop();
    void recover();
    bool allowChange(const nds::state_t, const nds::state_t, const nds::state_t);

    /* Variables for parameters */
    double  m_linConvFactor;
    double  m_linConvOffset;
    int32_t m_decimationFactor;
    int32_t m_decimationOffset;
    double  m_gain;
    int32_t m_pattern;

    /* Flags for when parameters are changed */
    double m_linConvFactorNext;
    double m_linConvOffsetNext;
    int32_t m_decimationFactorNext;
    int32_t m_decimationOffsetNext;
    double  m_gainNext;
    int32_t m_patternNext;

    bool m_linConvFactorChanged;
    bool m_linConvOffsetChanged;
    bool m_decimationFactorChanged;
    bool m_decimationOffsetChanged;
    bool m_gainChanged;
    bool m_patternChanged;

    /* PV for channel data */
    nds::PVDelegateIn<std::vector<double> > m_dataPV;
    /* PVs for readback values */
    nds::PVDelegateIn<double> m_linConvFactorPV;
    nds::PVDelegateIn<double> m_linConvOffsetPV;
    nds::PVDelegateIn<std::int32_t> m_decimationFactorPV;
    nds::PVDelegateIn<std::int32_t> m_decimationOffsetPV;
    nds::PVDelegateIn<double> m_gainPV;
    nds::PVDelegateIn<std::int32_t> m_patternPV;
};

#endif /* IFCDAQAICHANNEL_H */
