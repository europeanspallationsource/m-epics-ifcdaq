#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "IFCDAQ.h"
#include "IFCDAQAIChannelGroup.h"
#include "IFCDAQAIChannel.h"

IFCDAQAIChannelGroup::IFCDAQAIChannelGroup(const std::string& name, nds::Node& parentNode, ifcdaqdrv_usr & deviceUser) :
    m_node(nds::Port(name, nds::nodeType_t::generic)),
    m_deviceUser(deviceUser),
    m_nSamples(1024),
    m_triggerThreshold(0),
    m_triggerRepeat(-1),
    m_triggerSource(0),
    m_triggerDelay(0),
    m_decimation(1),
    m_averaging(1),
    m_nSamplesChanged(true),
    m_sampleRateChanged(false),
    m_triggerThresholdChanged(true),
    m_triggerRepeatChanged(true),
    m_triggerSourceChanged(true),
    m_triggerDelayChanged(true),
    m_clockSourceChanged(true),
    m_clockFrequencyChanged(true),
    m_clockDivisorChanged(true),
    m_decimationChanged(true),
    m_averagingChanged(true),
    m_infoPV(nds::PVDelegateIn<std::string>("InfoMessage",
                                                   std::bind(&IFCDAQAIChannelGroup::getInfoMessage,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_nSamplesPV(nds::PVDelegateIn<std::int32_t>("nSamples-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getSamples,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_sampleRatePV(nds::PVDelegateIn<double>("SampleRate-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getSampleRate,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_triggerThresholdPV(nds::PVDelegateIn<double>("TriggerThreshold-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerThreshold,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_triggerEdgePV(nds::PVDelegateIn<std::int32_t>("TriggerEdge-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerEdge,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_triggerRepeatPV(nds::PVDelegateIn<std::int32_t>("TriggerRepeat-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerRepeat,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_triggerSourcePV(nds::PVDelegateIn<std::int32_t>("TriggerSource-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerSource,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_triggerDelayPV(nds::PVDelegateIn<std::int32_t>("TriggerDelay-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerDelay,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_triggerCounterPV(nds::PVVariableIn<std::int32_t>("TriggerCounter-RB")),
    m_clockSourcePV(nds::PVDelegateIn<std::int32_t>("ClockSource-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getClockSource,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_clockFrequencyPV(nds::PVDelegateIn<double>("ClockFrequency-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getClockFrequency,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_clockDivisorPV(nds::PVDelegateIn<std::int32_t>("ClockDivisor-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getClockDivisor,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_decimationPV(nds::PVDelegateIn<std::int32_t>("Decimation-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getDecimation,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2))),
    m_averagingPV(nds::PVDelegateIn<std::int32_t>("Averaging-RB",
                                                   std::bind(&IFCDAQAIChannelGroup::getAveraging,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)))
{
    ifcdaqdrv_status status;
    parentNode.addChild(m_node);

    status = ifcdaqdrv_get_nchannels(&m_deviceUser, &m_numChannels);
    IFCDAQNDS_STATUS_CHECK("get_nchannels", status);

    m_rawData = (int32_t *)calloc(m_numChannels * CHANNEL_SAMPLES_MAX, sizeof(int32_t));
    if(!m_rawData)
    {
        throw nds::NdsError("Out of memory");
    }

    for(size_t numChannel(0); numChannel != m_numChannels; ++numChannel)
    {
        std::ostringstream channelName;
        channelName << "CH" << numChannel;
        m_AIChannels.push_back(std::make_shared<IFCDAQAIChannel>(channelName.str(), m_node, numChannel, m_deviceUser, m_rawData, 10.0/SHRT_MAX));
    }

    // PVs for Number of Samples.
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("nSamples",
                                                   std::bind(&IFCDAQAIChannelGroup::setSamples,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getSamples,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_nSamplesPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_nSamplesPV);

    // PVs for Sample rate
    m_node.addChild(nds::PVDelegateOut<double>("SampleRate",
                                                   std::bind(&IFCDAQAIChannelGroup::setSampleRate,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getSampleRate,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_sampleRatePV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_sampleRatePV);

    // PVs for Trigger Source
    nds::enumerationStrings_t triggerSourceStrings;
    triggerSourceStrings.push_back("CH-0");
    triggerSourceStrings.push_back("CH-1");
    triggerSourceStrings.push_back("CH-2");
    triggerSourceStrings.push_back("CH-3");
    triggerSourceStrings.push_back("CH-4");
    triggerSourceStrings.push_back("CH-5");
    triggerSourceStrings.push_back("CH-6");
    triggerSourceStrings.push_back("CH-7");
    triggerSourceStrings.push_back("EXT-GPIO");
    triggerSourceStrings.push_back("EXT-VMEIO");
    triggerSourceStrings.push_back("Software");
    nds::PVDelegateOut<std::int32_t> node(nds::PVDelegateOut<std::int32_t>("TriggerSource",
                                                   std::bind(&IFCDAQAIChannelGroup::setTriggerSource,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerSource,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));
    node.setEnumeration(triggerSourceStrings);
    m_node.addChild(node);

    m_triggerSourcePV.setScanType(nds::scanType_t::interrupt);
    m_triggerSourcePV.setEnumeration(triggerSourceStrings);
    m_node.addChild(m_triggerSourcePV);

    // PVs for Trigger Threshold
    m_node.addChild(nds::PVDelegateOut<double>("TriggerThreshold",
                                                   std::bind(&IFCDAQAIChannelGroup::setTriggerThreshold,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerThreshold,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_triggerThresholdPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_triggerThresholdPV);

    // PVs for Trigger Edge
    nds::enumerationStrings_t triggerEdgeStrings;
    triggerEdgeStrings.push_back("Rising Edge");
    triggerEdgeStrings.push_back("Falling Edge");
    node = nds::PVDelegateOut<std::int32_t>("TriggerEdge",
                                            std::bind(&IFCDAQAIChannelGroup::setTriggerEdge,
                                                      this,
                                                      std::placeholders::_1,
                                                      std::placeholders::_2),
                                            std::bind(&IFCDAQAIChannelGroup::getTriggerEdge,
                                                      this,
                                                      std::placeholders::_1,
                                                      std::placeholders::_2));
    node.setEnumeration(triggerEdgeStrings);
    m_node.addChild(node);

    m_triggerEdgePV.setScanType(nds::scanType_t::interrupt);
    m_triggerEdgePV.setEnumeration(triggerEdgeStrings);
    m_node.addChild(m_triggerEdgePV);

    // PVs for Trigger Repeat
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("TriggerRepeat",
                                                   std::bind(&IFCDAQAIChannelGroup::setTriggerRepeat,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerRepeat,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_triggerRepeatPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_triggerRepeatPV);

    // PVs for Trigger delay
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("TriggerDelay",
                                                   std::bind(&IFCDAQAIChannelGroup::setTriggerDelay,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getTriggerDelay,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_triggerDelayPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_triggerDelayPV);

    // PV for Trigger Counter
    m_triggerCounterPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_triggerCounterPV);

    // PVs for Clock Source
    nds::enumerationStrings_t clockSourceStrings;
    clockSourceStrings.push_back("Internal");
    clockSourceStrings.push_back("External");
    node = nds::PVDelegateOut<std::int32_t>("ClockSource",
                                                   std::bind(&IFCDAQAIChannelGroup::setClockSource,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getClockSource,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2));
    node.setEnumeration(clockSourceStrings);
    m_node.addChild(node);

    m_clockSourcePV.setScanType(nds::scanType_t::interrupt);
    m_clockSourcePV.setEnumeration(clockSourceStrings);
    m_node.addChild(m_clockSourcePV);

    // PVs for Clock Frequency
    m_node.addChild(nds::PVDelegateOut<double>("ClockFrequency",
                                                   std::bind(&IFCDAQAIChannelGroup::setClockFrequency,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getClockFrequency,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_clockFrequencyPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_clockFrequencyPV);


    // PVs for Clock Divisor
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("ClockDivisor",
                                                   std::bind(&IFCDAQAIChannelGroup::setClockDivisor,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getClockDivisor,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_clockDivisorPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_clockDivisorPV);

    // PVs for Hardware Decimation
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("Decimation",
                                                   std::bind(&IFCDAQAIChannelGroup::setDecimation,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getDecimation,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_decimationPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_decimationPV);

    // PVs for Hardware averaging
    m_node.addChild(nds::PVDelegateOut<std::int32_t>("Averaging",
                                                   std::bind(&IFCDAQAIChannelGroup::setAveraging,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2),
                                                   std::bind(&IFCDAQAIChannelGroup::getAveraging,
                                                             this,
                                                             std::placeholders::_1,
                                                             std::placeholders::_2)));

    m_averagingPV.setScanType(nds::scanType_t::interrupt);
    m_node.addChild(m_averagingPV);


    // PV for debug/info messages.
    m_infoPV.setScanType(nds::scanType_t::interrupt);
    m_infoPV.setMaxElements(512);
    m_node.addChild(m_infoPV);

    // PVs for state machine.
    m_stateMachine = m_node.addChild(nds::StateMachine(true,
                                    std::bind(&IFCDAQAIChannelGroup::onSwitchOn, this),
                                    std::bind(&IFCDAQAIChannelGroup::onSwitchOff, this),
                                    std::bind(&IFCDAQAIChannelGroup::onStart, this),
                                    std::bind(&IFCDAQAIChannelGroup::onStop, this),
                                    std::bind(&IFCDAQAIChannelGroup::recover, this),
                                    std::bind(&IFCDAQAIChannelGroup::allowChange,
                                              this,
                                              std::placeholders::_1,
                                              std::placeholders::_2,
                                              std::placeholders::_3)));

    // Initialize some values from hardware
    ifcdaqdrv_get_clock_source(&deviceUser, (ifcdaqdrv_clock *)&m_clockSource);
    ifcdaqdrv_get_clock_frequency(&deviceUser, &m_clockFrequency);
    ifcdaqdrv_get_clock_divisor(&deviceUser, (uint32_t *)&m_clockDivisor);

    m_sampleRate = m_clockFrequency/m_clockDivisor;

    commitParameters();

}

/*
 * Round upwards to nearest power-of-2.
 */

static inline int ceil_pow2(unsigned number) {
    unsigned n = 1;
    unsigned i = number - 1;
    while(i) {
        n <<= 1;
        i >>= 1;
    }
    return n;
}

/*
 * Round upwards to nearest mibibytes
 */

static inline int ceil_mibi(unsigned number) {
    return (1 + (number-1) / (1024 * 1024)) * 1024 * 1024;
}

void IFCDAQAIChannelGroup::getTriggerButton(timespec* timespec, std::string *value)
{
}

void IFCDAQAIChannelGroup::getInfoMessage(timespec* timespec, std::string *value)
{
    std::ostringstream tmp;
    char manufacturer[100];
    ifcdaqdrv_get_manufacturer(&m_deviceUser, manufacturer, 100);
    char product_name[100];
    ifcdaqdrv_get_product_name(&m_deviceUser, product_name, 100);
    tmp << manufacturer << " " << product_name;
    *value = tmp.str();
}

void IFCDAQAIChannelGroup::getSamples(timespec* timespec, int32_t* value)
{
    *value = m_nSamples;
}

void IFCDAQAIChannelGroup::setSamples(const timespec& timespec, const int32_t& value)
{
    if(value < -m_triggerDelay) {
        IFCDAQNDS_MSGWRN("Number of samples has to fit all Trigger Delay samples");
        return;
    }
    if(value > CHANNEL_SAMPLES_MAX) {
        IFCDAQNDS_MSGWRN("Buffer not large enough for samples. Dynamic allocation not implemented yet.");
        return;
    }
    m_nSamples = value;
    m_nSamplesChanged = true;
    commitParameters();
}

#define MAX_SAMPLE_RATES 100

struct sample_rate {
    double   frequency;
    uint32_t divisor;
    uint32_t decimation;
    uint32_t average;
    double   sample_rate;
};

// First priority is sample_rate, second divisor
static int compare_sample_rates(const void *a, const void *b) {
    const struct sample_rate *da = (const struct sample_rate *) a;
    const struct sample_rate *db = (const struct sample_rate *) b;
    int32_t sample_diff = (da->sample_rate > db->sample_rate) - (da->sample_rate < db->sample_rate);
    if(!sample_diff) {
        return da->divisor - db->divisor;
    }
    return sample_diff;
}

void IFCDAQAIChannelGroup::getSampleRate(timespec* timespec, double* value) {
    *value = m_sampleRate;
}
void IFCDAQAIChannelGroup::setSampleRate(const timespec& timespec, const double& value)
{
    if(value < 0) {
        IFCDAQNDS_MSGWRN("Sample rate cannot be negative.");
        return;
    }

    m_sampleRate = value;
    m_sampleRateChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getTriggerSource(timespec* timespec, int32_t* value)
{
    *value = m_triggerSource;
}

void IFCDAQAIChannelGroup::setTriggerSource(const timespec& timespec, const int32_t& value)
{
    m_triggerSource = value;
    m_triggerSourceChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getTriggerThreshold(timespec* timespec, double* value)
{
    *value = m_triggerThreshold;
}

void IFCDAQAIChannelGroup::setTriggerThreshold(const timespec& timespec, const double& value)
{
    m_triggerThreshold = value;
    m_triggerThresholdChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getTriggerEdge(timespec* timespec, int32_t* value)
{
    *value = m_triggerEdge;
}

void IFCDAQAIChannelGroup::setTriggerEdge(const timespec& timespec, const int32_t& value)
{
    m_triggerEdge = value;
    m_triggerEdgeChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getTriggerRepeat(timespec* timespec, int32_t* value)
{
    *value = m_triggerRepeat;
}

void IFCDAQAIChannelGroup::setTriggerRepeat(const timespec& timespec, const int32_t& value)
{
    if(value < -1 || value == 0) {
        IFCDAQNDS_MSGWRN("Invalid Trigger Repeat value");
        return;
    }
    m_triggerRepeat = value;
    m_triggerRepeatChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getTriggerDelay(timespec* timespec, int32_t* value)
{
    *value = m_triggerDelay;
}

void IFCDAQAIChannelGroup::setTriggerDelay(const timespec& timespec, const int32_t& value)
{
    if(-value > m_nSamples){
        IFCDAQNDS_MSGWRN("Trigger Delay cannot exceed number of samples");
        return;
    }
    if(value > 0){
        IFCDAQNDS_MSGWRN("Positive Trigger Delay is not supported.");
        return;
    }
    m_triggerDelay = value;
    m_triggerDelayChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getClockSource(timespec* timespec, int32_t* value)
{
    *value = m_clockSource;
}

void IFCDAQAIChannelGroup::setClockSource(const timespec& timespec, const int32_t& value)
{
    m_clockSource = value;
    m_clockSourceChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getClockFrequency(timespec* timespec, double* value)
{
    *value = m_clockFrequency;
}

void IFCDAQAIChannelGroup::setClockFrequency(const timespec& timespec, const double& value)
{
    m_clockFrequency = value;
    m_clockFrequencyChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getClockDivisor(timespec* timespec, int32_t* value)
{
    *value = m_clockDivisor;
}

void IFCDAQAIChannelGroup::setClockDivisor(const timespec& timespec, const int32_t& value)
{
    m_clockDivisor = value;
    m_clockDivisorChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getDecimation(timespec* timespec, int32_t* value)
{
    *value = m_decimation;
}

void IFCDAQAIChannelGroup::setDecimation(const timespec& timespec, const int32_t& value)
{
    m_decimation = value;
    m_decimationChanged = true;
    commitParameters();
}

void IFCDAQAIChannelGroup::getAveraging(timespec* timespec, int32_t* value)
{
    *value = m_averaging;
}

void IFCDAQAIChannelGroup::setAveraging(const timespec& timespec, const int32_t& value)
{
    m_averaging = value;
    m_averagingChanged = true;
    commitParameters();
}

/* commit parameters to hardware.
 *
 * the scope application supports
 */

void IFCDAQAIChannelGroup::commitParameters(bool calledFromAcquisitionThread)
{
    struct timespec now = {0, 0};
    ifcdaqdrv_status status;

    clock_gettime(CLOCK_REALTIME, &now);

    /* If the call comes from the acquisition thread it is certain that the device
     * is not armed. Otherwise, only allow changes if device is in on or going to on. */

    if(!calledFromAcquisitionThread && (
            m_stateMachine.getLocalState() != nds::state_t::on &&
            m_stateMachine.getLocalState() != nds::state_t::stopping  &&
            m_stateMachine.getLocalState() != nds::state_t::initializing )) {
        return;
    }

    /*
     * The board has a circular buffer so that values before the trigger can be read out. This is specified as "number of
     * pre-trigger samples". The amount has to be an even eighth of the total number of samples and it cannot be all
     * samples.
     *
     * The user should be able to enter an arbitrary amount of samples and pre-trigger samples. Using the following
     * "algorithm" the device support will then try to find a valid configuration to get the requested values.
     *
     * 1. First we calculate an intial pre-trigger samples quotient (ptq) to estimate this eighth.
     * 2. If ptq is 8 (8 eighths are requested as pre trigger samples) then the amount of samples are doubled so that
     *    ptq is closer to 4 eighths.
     * 3. If we then cannot satisfy the amount of post trigger samples, the number of samples has to be doubled again.
     *
     * Obvioysly pre-trigger samples are not allowed to be larger than the total amount of samples. This is enforced
     * in the setters earlier.
     */

    uint32_t nchannels;
    uint32_t sample_size;
    ifcdaqdrv_get_nchannels(&m_deviceUser, &nchannels);
    ifcdaqdrv_get_resolution(&m_deviceUser, &sample_size);
    sample_size /= 8;

    if(m_nSamplesChanged || m_triggerDelayChanged) {
        m_nSamplesChanged = false;
        m_triggerDelayChanged = false;

        uint32_t nSamples = 0;
        if(m_nSamples <= 16*1024) {
            nSamples = ceil_pow2(m_nSamples);
        } else {
            // 1MiB gets us 65k samples (divide by nchannels and sample_size)
            nSamples = ceil_mibi(m_nSamples * nchannels * sample_size) / nchannels / sample_size;
        }
        uint32_t nPretrig = 0;
        if(nSamples < 1024) {
            nSamples = 1024;
        }
        if(m_triggerDelay < 0) {
            nPretrig = -m_triggerDelay;
        }

        /* Calculate initial ptq, ceil(8*(npretrig/nsamples)) */
        uint32_t ptq = (8 * nPretrig + nSamples - 1) / nSamples;

        /* The only valid values for ptq is 0..7. Double the number of samples to fit the requested number of
         * pre-trigger samples. */
        if(ptq >= 8) {
            ndsDebugStream(m_node) << "Doubling nsamples" << std::endl;
            nSamples *= 2;
            if(nSamples > 16*1024) {
                nSamples = ceil_mibi(nSamples * nchannels * sample_size) / nchannels / sample_size;
            }
            ptq = (8 * nPretrig + nSamples - 1) / nSamples;
        }

        /* Check that we get enough post-trigger samples. Double the number of samples to get the requested number of
         * post-trigger samples. */
        if((8-ptq) * nSamples / 8 < m_nSamples - nPretrig) {
            ndsDebugStream(m_node) << "Doubling nsamples" << std::endl;
            nSamples *= 2;
            if(nSamples > 16*1024) {
                nSamples = ceil_mibi(nSamples * nchannels * sample_size) / nchannels / sample_size;
            }
            ptq = (8 * nPretrig + nSamples - 1) / nSamples;
        }

        nPretrig = (nSamples * ptq) / 8;

        ndsDebugStream(m_node) << "Number of samples: " << nSamples << ", Number of pre-trigger samples: " << nPretrig << std::endl;

        status = ifcdaqdrv_set_nsamples(&m_deviceUser, nSamples);
        IFCDAQNDS_STATUS_CHECK("set_nsamples", status);
        if(status != status_success){
            std::ostringstream tmp;
            tmp << "To many samples requested, tried " << nSamples << " samples with " << nPretrig << " pre-trigger samples.";
            IFCDAQNDS_MSGWRN(tmp.str());
            /* Set the largets possible amount of samples. */
            do {
                nSamples >>= 1;
                status = ifcdaqdrv_set_nsamples(&m_deviceUser, nSamples);
            } while(status && nSamples>0);
            ifcdaqdrv_get_nsamples(&m_deviceUser, (uint32_t *)&m_nSamples);
            nPretrig = m_triggerDelay = 0;
        }

        m_nSamplesPV.push(now, m_nSamples);

        status = ifcdaqdrv_set_npretrig(&m_deviceUser, nPretrig);
        IFCDAQNDS_STATUS_CHECK("set_npretrig", status);
        if(status) {
            status = ifcdaqdrv_set_npretrig(&m_deviceUser, 0);
            m_triggerDelay = 0;
            m_triggerDelayPV.push(now, 0);
        } else {
            m_triggerDelayPV.push(now, m_triggerDelay);
        }

    }


    if(m_triggerThresholdChanged || m_triggerSourceChanged || m_triggerEdgeChanged) {
        m_triggerThresholdChanged = false;
        m_triggerSourceChanged = false;
        m_triggerEdgeChanged = false;

        ifcdaqdrv_trigger_type triggerType = ifcdaqdrv_trigger_none;
        int32_t threshold = 0;
        uint32_t mask = 0;
        uint32_t polarity = 0;

        switch(m_triggerSource) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            triggerType = ifcdaqdrv_trigger_frontpanel;
            mask = 1 << m_triggerSource;
            polarity = ((m_triggerEdge + 1) % 2) << m_triggerSource;
            struct timespec tmp_time;
            double linConvFactor;
            try {
                m_AIChannels.at(m_triggerSource)->getLinearConversionFactor(&tmp_time, &linConvFactor);
                threshold = m_triggerThreshold/linConvFactor;
            } catch (const std::out_of_range& oor) {
                // Nothing to do if channel doesn't exist.
            }
            break;
        case 8:
            triggerType = ifcdaqdrv_trigger_frontpanel;
            mask = 1 << 30;
            polarity = ((m_triggerEdge + 1) % 2) << 30;
            break;
        case 9:
            triggerType = ifcdaqdrv_trigger_backplane;
            break;
        case 10:
            triggerType = ifcdaqdrv_trigger_soft;
            break;
        default:
            triggerType = ifcdaqdrv_trigger_none;
        }
        status = ifcdaqdrv_set_trigger(&m_deviceUser, triggerType, threshold, mask, polarity);
        IFCDAQNDS_STATUS_CHECK("set_trigger", status);
        if(!status) {
            m_triggerThresholdPV.push(now, m_triggerThreshold);
            m_triggerSourcePV.push(now, m_triggerSource);
            m_triggerEdgePV.push(now, m_triggerEdge);
        }
    }

    if(m_triggerRepeatChanged && m_stateMachine.getLocalState() != nds::state_t::running) {
        m_triggerRepeatChanged = false;
        m_triggerRepeatPV.push(now, m_triggerRepeat);
    }

    if(m_clockSourceChanged) {
        m_clockSourceChanged = false;
        status = ifcdaqdrv_set_clock_source(&m_deviceUser, (ifcdaqdrv_clock)m_clockSource);
        IFCDAQNDS_STATUS_CHECK("set_clock_source", status);
        if(!status) {
            m_clockSourcePV.push(now, m_clockSource);
        }
    }

    if(m_clockFrequencyChanged) {
        m_clockFrequencyChanged = false;
        status = ifcdaqdrv_set_clock_frequency(&m_deviceUser, m_clockFrequency);
        IFCDAQNDS_STATUS_CHECK("set_clock_frequency", status);
        if(!status) {
            m_clockFrequencyPV.push(now, m_clockFrequency);
            m_sampleRate = m_clockFrequency/m_clockDivisor/m_averaging/m_decimation;
            m_sampleRatePV.push(now, m_sampleRate);
        }
    }

    if(m_clockDivisorChanged) {
        m_clockDivisorChanged = false;
        status = ifcdaqdrv_set_clock_divisor(&m_deviceUser, m_clockDivisor);
        IFCDAQNDS_STATUS_CHECK("set_clock_divisor", status);
        if(!status) {
            m_clockDivisorPV.push(now, m_clockDivisor);
            m_sampleRate = m_clockFrequency/m_clockDivisor/m_averaging/m_decimation;
            m_sampleRatePV.push(now, m_sampleRate);
        }
    }

    if(m_decimationChanged) {
        m_decimationChanged = false;
        status = ifcdaqdrv_set_decimation(&m_deviceUser, m_decimation);
        IFCDAQNDS_STATUS_CHECK("set_decimation", status);
        if(!status) {
            m_decimationPV.push(now, m_decimation);
            m_sampleRate = m_clockFrequency/m_clockDivisor/m_averaging/m_decimation;
            m_sampleRatePV.push(now, m_sampleRate);
        }
    }

    if(m_averagingChanged) {
        m_averagingChanged = false;
        status = ifcdaqdrv_set_average(&m_deviceUser, m_averaging);
        IFCDAQNDS_STATUS_CHECK("set_average", status);
        if(!status) {
            m_averagingPV.push(now, m_averaging);
            m_sampleRate = m_clockFrequency/m_clockDivisor/m_averaging/m_decimation;
            m_sampleRatePV.push(now, m_sampleRate);
        }
    }

    if(m_sampleRateChanged) {
        m_sampleRateChanged = false;
        struct sample_rate sample_rates[MAX_SAMPLE_RATES] = {};
        double   frequencies[5];
        size_t nfrequencies;
        uint32_t decimations[8];
        size_t ndecimations;
        uint32_t averages[8];
        size_t naverages;
        uint32_t *downsamples;
        size_t ndownsamples;
        uint32_t  divisor, div_min, div_max;
        uint32_t i, j, nsample_rates;
        int32_t k;
        double sample_rate;

        status = ifcdaqdrv_get_clock_divisor_range(&m_deviceUser, &div_min, &div_max);
        IFCDAQNDS_STATUS_CHECK("get_clock_divisor_limits", status);
        status = ifcdaqdrv_get_clock_frequencies_valid(&m_deviceUser, frequencies, sizeof(frequencies)/sizeof(double), &nfrequencies);
        IFCDAQNDS_STATUS_CHECK("get_clock_frequency_valid", status);
        status = ifcdaqdrv_get_decimations_valid(&m_deviceUser, decimations, sizeof(decimations)/sizeof(uint32_t), &ndecimations);
        IFCDAQNDS_STATUS_CHECK("get_hw_decimations", status);
        status = ifcdaqdrv_get_averages_valid(&m_deviceUser, averages, sizeof(averages)/sizeof(uint32_t), &naverages);
        IFCDAQNDS_STATUS_CHECK("get_hw_averages", status);


        /*
         * Try to find the combination of clock frequency, clock divisor, decimation and average which
         * is closest (but higher) to the requested sample rate.
         *
         * The algorithm is as follows:
         * 1. For every available clock frequency
         *      For every available downsample (decimation and average)
         *         Start with the highest divisor and test all divisors until there is a sample rate higher than requested.
         *         If such a sample rate is found, add it to the list of sample rates.
         * 2. Sort list of sample rates. Lowest sample rate first. If equal prioritize lowest clock divisor.
         * 3. Pick the first combination in the list.
         */

        nsample_rates = 0;
        for(i = 0; i < nfrequencies; ++i) {
            for(j = 0; j < 2; ++j) {
                if (j == 0) {
                    downsamples = decimations;
                    ndownsamples = ndecimations;
                } else {
                    downsamples = averages;
                    ndownsamples = naverages;
                }
                for(k = ndownsamples - 1; k >= 0 ; --k) {
                    sample_rates[nsample_rates].frequency = frequencies[i];
                    sample_rates[nsample_rates].divisor = div_min;
                    sample_rates[nsample_rates].sample_rate = frequencies[i] / downsamples[k] / div_min;
                    sample_rates[nsample_rates].decimation = 1;
                    sample_rates[nsample_rates].average = 1;
                    for(divisor = div_max; divisor >= div_min; --divisor) {
                        sample_rate = frequencies[i] / downsamples[k] / divisor;
                        ndsDebugStream(m_node) << "Try Frequency: " << frequencies[i]
                                               << ", Divisor: "     << divisor
                                               << ", Downsample: "  << downsamples[i]
                                               << " : "             << sample_rate << std::endl;
                        if(sample_rate >= m_sampleRate) {
                            sample_rates[nsample_rates].frequency = frequencies[i];
                            sample_rates[nsample_rates].divisor = divisor;
                            sample_rates[nsample_rates].sample_rate = sample_rate;
                            if(j == 0) {
                                sample_rates[nsample_rates].decimation = downsamples[k];
                            } else {
                                sample_rates[nsample_rates].average = downsamples[k];
                            }
                            ndsDebugStream(m_node) << "OK Frequency: "  << sample_rates[nsample_rates].frequency
                                                   << ", Divisor: "     << sample_rates[nsample_rates].divisor
                                                   << ", Decimation: "  << sample_rates[nsample_rates].decimation
                                                   << ", Average: "     << sample_rates[nsample_rates].average
                                                   << ". Sample Rate: " << sample_rates[nsample_rates].sample_rate << std::endl;
                            nsample_rates++;
                            break;
                        }
                    }
                }
            }
        }

        // Sort lowest sample rates firsts.
        qsort(sample_rates, nsample_rates, sizeof(struct sample_rate), compare_sample_rates);

        ndsInfoStream(m_node) << "Will set Frequency: " << sample_rates[0].frequency
                              << ", Divider: "          << sample_rates[0].divisor
                              << ", Decimation: "       << sample_rates[0].decimation
                              << ", Average: "          << sample_rates[0].average
                              << ". Sample Rate: "      << sample_rates[0].sample_rate << std::endl;

        status = ifcdaqdrv_set_clock_frequency(&m_deviceUser, sample_rates[0].frequency);
        IFCDAQNDS_STATUS_CHECK("set_clock_frequency", status);
        status = ifcdaqdrv_set_clock_divisor(&m_deviceUser, sample_rates[0].divisor);
        IFCDAQNDS_STATUS_CHECK("set_clock_divisor", status);
        if(sample_rates[0].decimation > 1) {
            status = ifcdaqdrv_set_average(&m_deviceUser, 1);
            IFCDAQNDS_STATUS_CHECK("set_average", status);
            status = ifcdaqdrv_set_decimation(&m_deviceUser, sample_rates[0].decimation);
            IFCDAQNDS_STATUS_CHECK("set_decimation", status);
        } else {
            status = ifcdaqdrv_set_decimation(&m_deviceUser, 1);
            IFCDAQNDS_STATUS_CHECK("set_decimation", status);
            status = ifcdaqdrv_set_average(&m_deviceUser, sample_rates[0].average);
            IFCDAQNDS_STATUS_CHECK("set_average", status);
        }
        m_sampleRatePV.push(now, sample_rates[0].sample_rate);
        m_clockFrequencyPV.push(now, sample_rates[0].frequency);
        m_clockDivisorPV.push(now, (int32_t)sample_rates[0].divisor);
        m_averagingPV.push(now, (int32_t)sample_rates[0].average);
        m_decimationPV.push(now, (int32_t)sample_rates[0].decimation);
    }
}

void IFCDAQAIChannelGroup::onSwitchOn()
{
    // Enable all channels
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::on);
    }
    commitParameters();
}

void IFCDAQAIChannelGroup::onSwitchOff()
{
    // Disable all channels
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::off);
    }
}

void IFCDAQAIChannelGroup::onStart()
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);

    if(!m_nSamples) {
        m_infoPV.push(now, std::string("Samples is 0, won't start."));
        throw nds::StateMachineRollBack("Samples is 0, won't start.");
    }

    if(!m_triggerRepeat) {
        m_infoPV.push(now, std::string("Trigger Repeat is 0, won't start."));
        throw nds::StateMachineRollBack("Trigger Repeat is 0, won't start.");
    }

    /* Start all Channels */
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::running);
    }

    // Start data acquisition
    m_stop = false;
    m_acquisitionThread = m_node.runInThread("AcquisitionLoop", std::bind(&IFCDAQAIChannelGroup::acquisitionLoop, this, m_triggerRepeat));
}

void IFCDAQAIChannelGroup::onStop()
{
    m_stop = true;
    ifcdaqdrv_disarm_device(&m_deviceUser);

    m_acquisitionThread.join();
    // Stop channels
    for(auto const& channel: m_AIChannels) {
        channel->setState(nds::state_t::on);
    }

    commitParameters();
}

void IFCDAQAIChannelGroup::recover()
{
    throw nds::StateMachineRollBack("Cannot recover");
}

bool IFCDAQAIChannelGroup::allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal)
{
    return true;
}

void IFCDAQAIChannelGroup::acquisitionLoop(int32_t repeat)
{
    ndsInfoStream(m_node) << "Acquisition started, iterations: " << repeat << std::endl;
    struct timespec now;
    ifcdaqdrv_status status;
    uint32_t nSamplesHw = 0;
    int32_t nSamples = 0;
    uint32_t nPretrigHw = 0;
    int32_t nPretrig = 0;

    clock_gettime(CLOCK_REALTIME, &now);
    m_triggerCounterPV.push(now, 0);

    for(int32_t i = 0; repeat < 0 || i != repeat; ++i)
    {
        ifcdaqdrv_get_nsamples(&m_deviceUser, &nSamplesHw);
        ifcdaqdrv_get_npretrig(&m_deviceUser, &nPretrigHw);
        nSamples = m_nSamples < (int32_t)nSamplesHw ? m_nSamples : nSamplesHw;
        nPretrig = -m_triggerDelay;
        if(m_stop) {
            break;
        }
        status = ifcdaqdrv_arm_device(&m_deviceUser);
        IFCDAQNDS_STATUS_CHECK("arm_device", status);

        ndsDebugStream(m_node) << "Number of samples in HW: " << nSamplesHw << std::endl;
        status = ifcdaqdrv_wait_acq_end(&m_deviceUser);
        if(status == status_cancel) {
            ndsInfoStream(m_node) << "Acquisition cancelled." << std::endl;
            break;
        }
        else if(status != status_success) {
            m_stateMachine.setState(nds::state_t::fault);
            break;
        }
        clock_gettime(CLOCK_REALTIME, &now);
        m_triggerCounterPV.push(now, i + 1);
        ifcdaqdrv_read_ai(&m_deviceUser, m_rawData);
        for(auto const& channel: m_AIChannels) {
            //std::cout << "read ch" << channel->m_channelNum << std::endl;
            //ifcdaqdrv_read_ai_ch(&m_deviceUser, channel->m_channelNum, m_rawData + channel->m_channelNum * CHANNEL_SAMPLES_MAX);
            channel->read(m_rawData + (channel->m_channelNum * nSamplesHw), nSamples, nSamplesHw, nPretrig, nPretrigHw);
            /* Copy data */
            //std::cerr << channel << std::endl;
        }
        commitParameters(true);

    }

    ndsInfoStream(m_node) << "Acquisition finished." << std::endl;

    try {
        m_stateMachine.setState(nds::state_t::on);
    } catch (nds::StateMachineNoSuchTransition error) {
        /* We are probably already in "stopping", no need to panic... */
    }

}
