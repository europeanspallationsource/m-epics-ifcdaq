epicsEnvSet(PREFIX, "IFCACQ420")
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 400000000)
# Set maximum number of samples
epicsEnvSet(NELM, 1000000)

require ifcdaq,niklasclaesson

ndsCreateDevice(ifcdaq, ${PREFIX}, card=0, fmc=2)

dbLoadRecords(IFCDAQAIChannelGroup.template, "PREFIX=${PREFIX},CH_GRP_ID=AI")
dbLoadRecords(IFCDAQAIChannel.template,      "PREFIX=${PREFIX},CH_GRP_ID=AI,CH_ID=CH0")
dbLoadRecords(IFCDAQAIChannel.template,      "PREFIX=${PREFIX},CH_GRP_ID=AI,CH_ID=CH1")
dbLoadRecords(IFCDAQAIChannel.template,      "PREFIX=${PREFIX},CH_GRP_ID=AI,CH_ID=CH2")
dbLoadRecords(IFCDAQAIChannel.template,      "PREFIX=${PREFIX},CH_GRP_ID=AI,CH_ID=CH3")

# ndsSetLogLevelDebug ${PREFIX}
