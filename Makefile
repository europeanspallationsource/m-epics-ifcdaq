include ${EPICS_ENV_PATH}/module.Makefile

EXCLUDE_ARCHS = centos

# We need an explicit dependency on nds3epics.
USR_DEPENDENCIES = nds3epics

USR_CXXFLAGS = -std=c++0x

OPIS = opi

#CROSS_OPT = NO
